Key,File,Type,UsedInMainMenu,NoTranslate,english,Context / Alternate Text,german,latam,french,italian,japanese,koreana,polish,brazilian,russian,turkish,schinese,tchinese,spanish

OldAKM556,items,Item,,,"АКМ 5,56"
OldAKM556Desc,items,Item,,,"АКМ под патрон 5,56"

OldAKM,items,Item,,,"АКМ 7,62"
OldAKMDesc,items,Item,,,"АКМ стандартный автомат Калашникова разработанный под патрон 7,62"

OldAKM308,items,Item,,,"АКМ .308"
OldAKM308Desc,items,Item,,,"АКМ под патрон .308"

ammo556,items,Item,,,"Патрон 5,56"
ammo556Desc,items,Item,,,"Патрон 5,56"

ammo556HP,items,Item,,,"Патрон 5,56 Экспансивный"
ammo556HPDesc,items,Item,,,Патрон 5,56 Экспансивный

ammo556AP,items,Item,,,"Патрон 5,56 Бронебойный"
ammo556APDesc,items,Item,,,"Патрон 5,56 Бронебойный"

ammoBundle556,items,Item,,,"Коробка патронов 5,56"
ammoBundle556Desc,items,Item,,,"Коробка патронов 5,56"

ammoBundle556HP,items,Item,,,"Коробка экспансивных патронов 5,56"
ammoBundle556HPDesc,items,Item,,,"Коробка экспансивных патронов 5,56"

ammoBundle556AP,items,Item,,,"Коробка бронебойных патронов 5,56"
ammoBundle556APDesc,items,Item,,,"Коробка бронебойных патронов 5,56"

ammo10mm,items,Item,,,"Патрон 10мм"
ammo10mmDesc,items,Item,,,"Патрон 10мм"

ammo10mmHP,items,Item,,,"Патрон 10мм Экспансивный"
ammo10mmHPDesc,items,Item,,,Патрон 10мм Экспансивный

ammo10mmAP,items,Item,,,"Патрон 10мм Бронебойный"
ammo10mmAPDesc,items,Item,,,"Патрон 10мм Бронебойный"

ammoBundle10mm,items,Item,,,"Коробка патронов 10мм"
ammoBundle10mmDesc,items,Item,,,"Коробка патронов 10мм"

ammoBundle10mmHP,items,Item,,,"Коробка экспансивных патронов 10мм"
ammoBundle10mmHPDesc,items,Item,,,"Коробка экспансивных патронов 10мм"

ammoBundle10mmAP,items,Item,,,"Коробка бронебойных патронов 10мм"
ammoBundle10mmAPDesc,items,Item,,,"Коробка бронебойных патронов 10мм"

ammo50ae,items,Item,,,"Патрон .50 АЕ"
ammo50aeDesc,items,Item,,,"Патрон .50 АЕ"

ammo50aeHP,items,Item,,,"Патрон .50 АЕ Усиленный"
ammo50aeHPDesc,items,Item,,,"Патрон .50 АЕ Усиленный"

ammo50aeAP,items,Item,,,"Патрон .50 АЕ Бронебойный"
ammo50aeAPDesc,items,Item,,,"Патрон .50 АЕ Бронебойный"

ammo76251NATO,items,Item,,,"Патрон .308"
ammo76251NATODesc,items,Item,,,"Патрон .308 или 7,62x51 НАТО Пулеметный"

ammo76251NATOHP,items,Item,,,"Патрон .308 Усиленный"
ammo76251NATOHPDesc,items,Item,,,"Патрон .308 или 7,62x51 НАТО Пулеметный Усиленный"

ammo76251NATOAP,items,Item,,,"Патрон .308 Бронебойный"
ammo76251NATOAPDesc,items,Item,,,"Патрон .308 или 7,62x51 НАТО Пулеметный Бронебойный"

ammo76254,items,Item,,,"Патрон 7,62x54 Винтовочный"
ammo76254Desc,items,Item,,,"Патрон 7,62x54 Винтовочный"

ammo76254HP,items,Item,,,"Патрон 7,62x54 Винтовочный Усиленный"
ammo76254HPDesc,items,Item,,,"Патрон 7,62x54 Винтовочный Усиленный"

ammo76254AP,items,Item,,,"Патрон 7,62x54 Винтовочный Бронебойный"
ammo76254APDesc,items,Item,,,"Патрон 7,62x54 Винтовочный Бронебойный"

Saiga,items,Item,,,"Сайга 12"
SaigaDesc,items,Item,,,"Сайга 12 Российский карабин под патрон дробовика"

SaigaSchematic,items,Item,,,"Схема Сайга 12"
SaigaSchematicDesc,items,Item,,,"Схема самозарядного карабина Сайга 12"

ammo4570,items,Item,,,"Патрон 45-70"
ammo4570Desc,items,Item,,,"Патрон 45-70"

ammo4570HP,items,Item,,,"Патрон 45-70 Экспансивный"
ammo4570HPDesc,items,Item,,,Патрон 45-70 Экспансивный

ammo4570AP,items,Item,,,"Патрон 45-70 Бронебойный"
ammo4570APDesc,items,Item,,,"Патрон 45-70 Бронебойный"

4570Rifle,items,Item,,,"Рычажная винтовка 45-70"
4570RifleDesc,items,Item,,,"Рычажная винтовка разработанная под патрон калибра 45-70"

ammo357,items,Item,,,"Патрон .357 Магнум"
ammo357Desc,items,Item,,,"Патрон .357 Магнум"

ammo357HP,items,Item,,,"Патрон .357 Магнум Экспансивный"
ammo357HPDesc,items,Item,,,"Патрон .357 Магнум Экспансивный"

ammo357AP,items,Item,,,"Патрон .357 Магнум Бронебойный"
ammo357APDesc,items,Item,,,"Патрон .357 Магнум Бронебойный"

357Rifle,items,Item,,,"Рычажная винтовка .357 Магнум"
357RifleDesc,items,Item,,,"Рычажная винтовка разработанная под патрон калибра 357 Магнум"

357Pistol,items,Item,,,"Револьвер .357 Магнум"
357PistolDesc,items,Item,,,"Револьвер разработанный под патрон калибра 357 Магнум"

357Desert,items,Item,,,"Пустынный Орёл .357 Магнум"
357DesertDesc,items,Item,,,"Пустынный Орёл разработанный под патрон калибра 357 Магнум"

44Desert,items,Item,,,"Пустынный Орёл .44 Магнум"
44DesertDesc,items,Item,,,"Пустынный Орёл разработанный под патрон калибра 44 Магнум"

SMG9mm,items,Item,,,"МП-5"
SMG9mmDesc,items,Item,,,"МП-5 разработанный под патрон калибра 9мм"

50BMGRifle,items,Item,,,"Ружье калибра 50БМГ"
50BMGRifleDesc,items,Item,,,"Ружье кустарным образом модернезированное под патрон калибра 50БМГ"

76251Rifle,items,Item,,,"Ружье калибра .308"
76251RifleDesc,items,Item,,,"Ружье кустарным образом модернезированное под патрон калибра .308"

76254Rifle,items,Item,,,"Ружье калибра 7,62x54"
76254RifleDesc,items,Item,,,"Ружье кустарным образом модернезированное под патрон калибра 7,62x54"

resiver357,items,Item,,,"Ресивер .357 Магнум"
resiver357Desc,items,Item,,,"Ресивер используется для смены типа патронов оружия"

resiver44,items,Item,,,"Ресивер .44 Магнум"
resiver44Desc,items,Item,,,"Ресивер используется для смены типа патронов оружия"

resiver50AE,items,Item,,,"Ресивер .50 АЕ"
resiver50AEDesc,items,Item,,,"Ресивер используется для смены типа патронов оружия"

resiver9mm,items,Item,,,"Ресивер 9мм"
resiver9mmDesc,items,Item,,,"Ресивер используется для смены типа патронов оружия"

resiver10mm,items,Item,,,"Ресивер 10мм"
resiver10mmDesc,items,Item,,,"Ресивер используется для смены типа патронов оружия"

resiver50BMG,items,Item,,,"Ресивер 50БМГ"
resiver50BMGDesc,items,Item,,,"Ресивер используется для смены типа патронов оружия"

resiver45-70,items,Item,,,"Ресивер .45-70"
resiver45-70Desc,items,Item,,,"Ресивер используется для смены типа патронов оружия"

resiver762x54,items,Item,,,"Ресивер 7,62x54"
resiver762x54Desc,items,Item,,,"Ресивер используется для смены типа патронов оружия"

resiver762x51,items,Item,,,"Ресивер .308"
resiver762x51Desc,items,Item,,,"Ресивер используется для смены типа патронов оружия"

resiver762x39,items,Item,,,"Ресивер 7,62x39"
resiver762x39Desc,items,Item,,,"Ресивер используется для смены типа патронов оружия"

StartRifle,items,Item,,,"Старый Винчестер"
StartRifleDesc,items,Item,,,"Похоже твоей винтовке сильно досталось, врятли ее удастся починить, но стрелять она все еще может"

HuntingKnife,items,Item,,,"Старый Охотничий нож"
HuntingKnifeDesc,items,Item,,,"Это старый повидавший времена тупой охотничий нож, врятли его удастся починить, но резать и колоть им что то да возможно"

StartPistol,items,Item,,,"Старый Пистолет"
StartPistolDesc,items,Item,,,"Твой старый пистолет успел многое повидать и уже практически никуда не годится, врятли его удастся починить, но отстрелять пару магазинов все еще может"

The tramp starter kit,items,Item,,,"Набор Бродяги"
The tramp starter kitDesc,items,Item,,,"Особо не надейся, содержимое сумки лишь отсрочит НЕИЗБЕЖНОЕ!"

Medic's starter kit,items,Item,,,"Набор Медика"
Medic's starter kitDesc,items,Item,,,"Особо не надейся, содержимое сумки лишь отсрочит НЕИЗБЕЖНОЕ!"

Scalpel,items,Item,,,"Скальпель"
ScalpelDesc,items,Item,,,"Для хирургических задач уже не пригоден, но зарезать зомби весьма способен"

StarterKit,items,Item,,,"Стартовые наборы"
StarterKitDesc,items,Item,,,"В Меню рецептов выбери свой наиболее подобающий набор для выживания, каждый набор содержит в себе соответсвующее Классу содержимое!"

Police recruitment,items,Item,,,"Набор Полицейского"
Police recruitmentDesc,items,Item,,,"Особо не надейся, содержимое сумки лишь отсрочит НЕИЗБЕЖНОЕ!"

Soldier's Kit,items,Item,,,"Набор Солдата"
Soldier's KitDesc,items,Item,,,"Особо не надейся, содержимое сумки лишь отсрочит НЕИЗБЕЖНОЕ!"

DamagedAssaultRifle,items,Item,,,"Изношенная Штурмовая винтовка"
DamagedAssaultRifleDesc,items,Item,,,"Твой М-4 успел многое повидать и уже практически никуда не годится, врятли его удастся починить, но отстрелять пару магазинов все еще может"

ammoBundleShotgunShellsmall,items,Item,,,"Коробочка Патронов 12го Калибра"
ammoBundleShotgunShellsmallDesc,items,Item,,,"Коробочка Патронов 12го Калибра"

ammo4570Bundle,items,Item,,,"Коробочка Патронов 45-70"
ammo4570BundleDesc,items,Item,,,"Коробочка Патронов 45-70"

armorMilitaryHelmetOLD,items,Item,,,"Поврежденный Армейский Шлем"
armorMilitaryHelmetOLDDesc,items,Item,,,"Поврежденный Армейский Шлем"

armorMilitaryVestOLD,items,Item,,,"Поврежденный Армейский Жилет"
armorMilitaryVestOLDDesc,items,Item,,,"Поврежденный Армейский Жилет"

armorMilitaryGlovesOLD,items,Item,,,"Поврежденные Армейские Перчатки"
armorMilitaryGlovesOLDDesc,items,Item,,,"Поврежденные Армейские Перчатки"

armorMilitaryLegsOLD,items,Item,,,"Поврежденные Армейские Поножи"
armorMilitaryLegsOLDDesc,items,Item,,,"Поврежденные Армейские Поножи"

armorMilitaryBootsOLD,items,Item,,,"Поврежденные Армейские Бутсы"
armorMilitaryBootsOLDDesc,items,Item,,,"Поврежденные Армейские Бутсы"

Chef's Kit,items,Item,,,"Набор Повара"
Chef's KitDesc,items,Item,,,"Особо не надейся, содержимое сумки лишь отсрочит НЕИЗБЕЖНОЕ!"

Hunter's Kit,items,Item,,,"Набор Охотника"
Hunter's KitDesc,items,Item,,,"Особо не надейся, содержимое сумки лишь отсрочит НЕИЗБЕЖНОЕ!"

PoliceBaton,items,Item,,,"Полицейская Дубинка"
PoliceBatonDesc,items,Item,,,"Полицейская Дубинка в отличие от других, бьет быстрее и больнее, а так же обладает повышенной прочностью"

Farmer's kit,items,Item,,,"Набор Фермера"
Farmer's kitDesc,items,Item,,,"Особо не надейся, содержимое сумки лишь отсрочит НЕИЗБЕЖНОЕ!"

Miner's Kit,items,Item,,,"Набор Шахтера"
Miner's KitDesc,items,Item,,,"Особо не надейся, содержимое сумки лишь отсрочит НЕИЗБЕЖНОЕ!"

Mechanic's Kit,items,Item,,,"Набор Механика"
Mechanic's KitDesc,items,Item,,,"Особо не надейся, содержимое сумки лишь отсрочит НЕИЗБЕЖНОЕ!"

SilverNuggetBundle,items,Item,,,"Серебрянные самородки"
SilverNuggetBundleDesc,items,Item,,,"Когда начался апокалипсис тебе удалось извлечь из шахты особую пользу, не спеши продавать сие ресурс, возможно он пригодится для более полезного дела!"

GoldNuggetBundle,items,Item,,,"Золотые самородки"
GoldNuggetBundleDesc,items,Item,,,"Когда начался апокалипсис тебе удалось извлечь из шахты особую пользу, не спеши продавать сие ресурс, возможно он пригодится для более полезного дела!"

RawDiamondBundle,items,Item,,,"Неограненные алмазы"
RawDiamondBundleDesc,items,Item,,,"Когда начался апокалипсис тебе удалось извлечь из шахты особую пользу, не спеши продавать сие ресурс, возможно он пригодится для более полезного дела!"

m110Rifle,items,Item,,,"М110"
m110RifleDesc,items,Item,,,"Да-да, это та самая снайперка из 19 альфы! P.S: Разрабы с*ки, ибо мне пришлось ее восстанавливать!"

m110RifleSchematic,items,Item,,,"Схема Снайперской винтовки М110"

resiver556,items,Item,,,"Ресивер 5,56"
resiver556Desc,items,Item,,,"Ресивер используется для смены типа патронов оружия"

AK47556,items,Item,,,"АК-47 Калибра 5,56"
AK47556Desc,items,Item,,,"АК-47 переработанный под патрон 5,56"

AK47308,items,Item,,,"АК-47 Калибра .308"
AK47308Desc,items,Item,,,"АК-47 переработанный под патрон .308"

m110Rifle762x39,items,Item,,,"М110 Калибра 7,62x39"
m110Rifle762x39Desc,items,Item,,,"М110 переработанный под патрон 7,62x39"

m110Rifle762x54,items,Item,,,"М110 Калибра 7,62x54"
m110Rifle762x54Desc,items,Item,,,"М110 переработанный под патрон 7,62x54"
